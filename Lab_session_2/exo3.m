% Name & Surname : Mokhles BOUZAIEN
% E-mail : mokhles.bouzaien@eurecom.fr

% Exercice 3

% Create the main image
square = uint8(ones(256,256));
square(:,:) = 64;
square(97:97+63,97:97+63) = 192;

% Create a random image
rand_image = randi([0,255],256);

% Adding both images
noisy_image = double(square) + rand_image;


% Create median filtered image
filtered_image_3x3 = medfilt2(noisy_image);
filtered_image_9x9 = medfilt2(noisy_image,[9 9]);
filtered_image_15x15 = medfilt2(noisy_image,[15 15]);


figure
subplot(2,3,1);
imshow(square);
title('Original Image');
subplot(2,3,2);
imshow(rand_image,[]);
title('Random Image');
subplot(2,3,3);
imshow(noisy_image,[]);
title('Noisy Image');
subplot(2,3,4);
imshow(filtered_image_3x3,[]);
title('3x3 Median Filter');
subplot(2,3,5);
imshow(filtered_image_9x9,[]);
title('9x9 Median Filter');
subplot(2,3,6);
imshow(filtered_image_15x15,[]);
title('15x15 Median Filter');

% 3D Visualization
figure
subplot(1,3,1);
mesh(filtered_image_3x3);
subplot(1,3,2);
mesh(filtered_image_9x9);
subplot(1,3,3);
mesh(filtered_image_15x15);
colormap(jet)

% Comments
% As we can see in the 3D visualization, this filter allows to eliminate
% the noise (the surface becomes smoother) and keep the important details
% such as edges (we can clearly see the difference between the central and
% the dge area.