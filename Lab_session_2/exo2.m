% Name & Surname : Mokhles BOUZAIEN
% E-mail : mokhles.bouzaien@eurecom.fr

% Exercice 2

% Create the main image
square = uint8(ones(256,256));
square(:,:) = 64;
square(97:97+63,97:97+63) = 192;
fft_original = fft2(square);
fftshift_original = fftshift(fft_original);

% Create a random image
rand_image = randi([0,255],256);

% Adding both images
noisy_image = double(square) + rand_image;
fft_noisy = fft2(noisy_image);
fftshift_noisy = fftshift(fft_noisy);

% Creating the filters with different sizes
h_1 = fspecial('average',3);
h_2 = fspecial('average',9);
h_3 = fspecial('average',15);

filtered_image_1 = filter2(h_1,noisy_image);
filtered_image_2 = filter2(h_2,noisy_image);
filtered_image_3 = filter2(h_3,noisy_image);

fft_image_1 = fft2(filtered_image_1);
fftshift_image_1 = fftshift(fft_image_1);
fft_image_2 = fft2(filtered_image_2);
fftshift_image_2 = fftshift(fft_image_2);
fft_image_3 = fft2(filtered_image_3);
fftshift_image_3 = fftshift(fft_image_3);

figure
subplot(2,3,1);
imshow(square);
title('Original Image');
subplot(2,3,2);
imshow(rand_image,[]);
title('Random Image');
subplot(2,3,3);
imshow(noisy_image,[]);
title('Noisy Image');
subplot(2,3,4);
imshow(filtered_image_1,[]);
title('3x3 Average Filter');
subplot(2,3,5);
imshow(filtered_image_2,[]);
title('9x9 Average Filter');
subplot(2,3,6);
imshow(filtered_image_3,[]);
title('15x15 Average Filter');

figure
subplot(1,5,1);
imshow(log(abs(fftshift_original)+1),[]);
title('Original Image FFT');
subplot(1,5,2);
imshow(log(abs(fftshift_noisy)+1),[]);
title('Noisy Image FFT');
subplot(1,5,3);
imshow(log(abs(fftshift_image_1)+1),[]);
title('3x3 Average Filtered FFT');
subplot(1,5,4);
imshow(log(abs(fftshift_image_2)+1),[]);
title('9x9 Average Filtered FFT');
subplot(1,5,5);
imshow(log(abs(fftshift_image_3)+1),[]);
title('15x15 Average Filtered FFT');

% Comments
% By increasing the filter size, we can eliminate more and more noise (both
% areas of the image become uniform) but we also lose some important
% details such as edges which become not clear in the last filtered image
% (15x15 filter). We can see those changes in the frequency domain : whene
% we increase the filter size high frequencies are clearly cancelled.