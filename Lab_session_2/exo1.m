% Name & Surname : Mokhles BOUZAIEN
% E-mail : mokhles.bouzaien@eurecom.fr

% Exercice 1

% lena = imread('\\datas\teaching\courses\image\TpGettingStarted\public\Images\lena.tif');
lena = imread('lena.png');

% C. Questions
% * The filter is a matrix of dimensions dim(1) lines and dim(2) colums
% (dim(i) stands for the element number i of the dim array).
% * fcoupure is the threshold to which we have to compare the value of the
% obtained R matrix and then extract the indices that have values less than
% this threshold (for a low pass filter).
% * We find the indices of R where the value is GREATER than fcoupure (e.g.
% indices = find(R>fcoupure).


% Apply FFT
fft_lena = fft2(lena);
fftshift_lena = fftshift(fft_lena);

% Create Low Filter Pass masks with different thresholds
LFP_mask_1 = freqLPF([512 512], 0.1);
LFP_mask_2 = freqLPF([512 512], 0.2);
LFP_mask_3 = freqLPF([512 512], 0.3);

% Apply the filter
low_filtered_lena_1 = LFP_mask_1 .* fftshift_lena;
low_filtered_lena_2 = LFP_mask_2 .* fftshift_lena;
low_filtered_lena_3 = LFP_mask_3 .* fftshift_lena;

% Apply IFFTShift
ifftshift_low_filtered_lena_1 = ifftshift(low_filtered_lena_1);
ifftshift_low_filtered_lena_2 = ifftshift(low_filtered_lena_2);
ifftshift_low_filtered_lena_3 = ifftshift(low_filtered_lena_3);

% Apply IFFT
ifft_low_filtered_lena_1 = ifft2(ifftshift_low_filtered_lena_1);
ifft_low_filtered_lena_2 = ifft2(ifftshift_low_filtered_lena_2);
ifft_low_filtered_lena_3 = ifft2(ifftshift_low_filtered_lena_3);


% Create High Filter Pass masks with different thresholds
HFP_mask_1 = freqHPF([512 512], 0.1);
HFP_mask_2 = freqHPF([512 512], 0.2);
HFP_mask_3 = freqHPF([512 512], 0.3);

% Apply the filter
high_filtered_lena_1 = HFP_mask_1 .* fftshift_lena;
high_filtered_lena_2 = HFP_mask_2 .* fftshift_lena;
high_filtered_lena_3 = HFP_mask_3 .* fftshift_lena;

% Apply IFFTShift
ifftshift_high_filtered_lena_1 = ifftshift(high_filtered_lena_1);
ifftshift_high_filtered_lena_2 = ifftshift(high_filtered_lena_2);
ifftshift_high_filtered_lena_3 = ifftshift(high_filtered_lena_3);

% Apply IFFT
ifft_high_filtered_lena_1 = ifft2(ifftshift_high_filtered_lena_1);
ifft_high_filtered_lena_2 = ifft2(ifftshift_high_filtered_lena_2);
ifft_high_filtered_lena_3 = ifft2(ifftshift_high_filtered_lena_3);


figure('Name','LFP, Threshold=0.1')
subplot(2,2,1);
imshow(lena);
title('Original Image');
subplot(2,2,2);
imshow(log(abs(fftshift_lena)+1),[]);
title('Spectrum');
subplot(2,2,3);
imshow(log(abs(low_filtered_lena_1)+1),[]);
title('Filtered Spectrum');
subplot(2,2,4);
imshow(ifft_low_filtered_lena_1,[])
title('Filtered Image');

figure('Name','LFP, Threshold=0.2')
subplot(2,2,1);
imshow(lena);
title('Original Image');
subplot(2,2,2);
imshow(log(abs(fftshift_lena)+1),[]);
title('Spectrum');
subplot(2,2,3);
imshow(log(abs(low_filtered_lena_2)+1),[]);
title('Filtered Spectrum');
subplot(2,2,4);
imshow(ifft_low_filtered_lena_2,[])
title('Filtered Image');

figure('Name','LFP, Threshold=0.3')
subplot(2,2,1);
imshow(lena);
title('Original Image');
subplot(2,2,2);
imshow(log(abs(fftshift_lena)+1),[]);
title('Spectrum');
subplot(2,2,3);
imshow(log(abs(low_filtered_lena_3)+1),[]);
title('Filtered Spectrum');
subplot(2,2,4);
imshow(ifft_low_filtered_lena_3,[])
title('Filtered Image');

% For all low pass filters, we are deleting high frequencies (which
% represent the edges and the noise). That's why the less the threshold is
% (we delete more high frequencies) the more edges disappear and the image
% become blur.

% We can do the exact same work for high pass filters
figure('Name','HFP, Threshold=0.1')
subplot(2,2,1);
imshow(lena);
title('Original Image');
subplot(2,2,2);
imshow(log(abs(fftshift_lena)+1),[]);
title('Spectrum');
subplot(2,2,3);
imshow(log(abs(high_filtered_lena_1)+1),[]);
title('Filtered Spectrum');
subplot(2,2,4);
imshow(ifft_high_filtered_lena_1,[]);
title('Filtered Image');

% Here, we only keep high frequencies. The result image only contain edges.