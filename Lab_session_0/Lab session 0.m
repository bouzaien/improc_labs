r = rand(10,10)
find(r<0.5)
size(r)

figure
x = [-pi:0.01:pi]
y = sin(x)
z = cos(x)
subplot(2,1,1)
plot(x,y)
subplot(2,1,2)
plot(x,z)

figure
x = [-pi:0.01:pi]
y = sin(x)
z = cos(x)
plot(x,y,'b')
hold
plot(x,z,'r')

img = imread('baboon.tif');
size(img)
img_double = double(img);
figure
imshow(img)
R = img(:,:,1)
G = img(:,:,2)
B = img(:,:,3)
figure
subplot(1,3,1);imshow(R)
subplot(1,3,2);imshow(B)
subplot(1,3,3);imshow(G)

figure
imshow(imresize(img,4,'bicubic'))


figure
[count,x] = imhist(img,256)
cum = cumsum(count)
plot(x,cum)

figure
imshow(img)
eqImg = histeq(img,256)
figure
imshow(eqImg)

img = imread('baboon.tif');
filtre = fspecial('gaussian',5,1);
imgf = filter2(filtre,rgb2gray(img));
figure;
imshow(imgf,[]);

img = imread('lena.tif');
imgnlf = nlfilter(rgb2gray(img),[3 3],'std2');
figure;
imshow(imgnlf,[]);

img = imread('baboon.tif');
mosaic = blkproc(img,[8 8],'unit8(mean(x(:))*ones(8,8))'); %#ok<DBLKPRC>
figure
size(mosaic)