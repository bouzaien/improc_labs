#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <stdlib.h>
#include <iostream>
#include <stdio.h>

using namespace std;
using namespace cv;

/// Global variables

Mat src, src_gray;
Mat dst, dst2, dst3, dst4, abs_dst4, detected_edges;

char* window_name0 = "Original Image";
char* window_name1 = "Grayscale Image";
char* window_name2 = "Image After Histogram Equalization";
char* remap_window1 = "Remap - upside down";
char* remap_window2 = "Remap - reflection in the x direction";
char* window_name4 = "Median Filtered Image";
char* window_name5 = "Gaussian Filtered Image";
char* window_name6 = "Laplace Demo";
char* window_name7 = "Sobel Demo - Simple Edge Detector";

int ddepth = CV_16S;
int scale = 1;
int delta = 0;
int kernel_size = 3;
char* window_name8 = "Laplace Demo";

char* window_name9 = "Sobel Demo - Simple Edge Detector";

int MAX_KERNEL_LENGTH = 6;


/** @function main */
int main( int argc, char** argv )
{

	if( argc != 2)
	{
		cout <<" Usage: display_image ImageToLoadAndDisplay" << endl;
		return -1;
	}

	// Call the appropriate function in OPENCV to load the image
	src = imread( "../imgs/Baboon.jpg" );
	if( !src.data )
		{ return -1; }

	// Create a window called "Original Image" and show original image
	imshow(window_name0, src);
	
	// Call the appropriate function in OPENCV to convert the image to grayscale
	cvtColor(src, src_gray, 7); // 7 for RGB_TO_GRAY

	// Create a window called "Grayscale Image" and show grayscale image
	imshow(window_name1, src_gray);

	// Apply histogram equalization to the grayscale image
	equalizeHist(src_gray, dst);
	
	// Create a window called "Image After Histogram Equalization" and show the image after histogram equalization
	imshow(window_name2, dst);

	// Apply remapping; first turn the image upside down and then reflect the image in the x direction

    // For this part, the upside down image and the flipped left image are created as the Mat variables "image_upsidedown" and "image_flippedleft". Also, map_x and map_y are created with the same size as equalized_image:
	Mat image_upsidedown;
	Mat image_flippedleft;

	Mat map_x, map_y;
	
	// Initialize images with the same size and type of Histogram Equalization output image
	image_upsidedown.create(dst.size(), dst.type());
	image_flippedleft.create(dst.size(), dst.type());

	map_x.create(dst.size(), CV_32FC1);
	map_y.create(dst.size(), CV_32FC1);

	// Apply upside down operation to the image for which histogram equalization is applied.
	for (int j=0; j<dst.cols; j++) {
		for (int i=0; i<dst.rows; i++) {
			map_x.at<float>(i, j) = (float)j;
			map_y.at<float>(i, j) = (float)(src.rows - i);
		}
	}
	remap(dst, image_upsidedown, map_x, map_y, CV_INTER_LINEAR);

	// Create a window called "Remap - upside down" and show the image after applying remapping - upside down
	imshow("Remap - upside down", image_upsidedown);


	// Apply reflection in the x direction operation to the image for which histogram equalization is applied.
	for (int j=0; j<dst.cols; j++) {
		for (int i = 0; i < dst.rows; i++) {
			map_x.at<float>(i, j) = (float)(src.cols - j);
			map_y.at<float>(i, j) = (float)i;
		}
	}
	remap(dst, image_flippedleft, map_x, map_y, CV_INTER_LINEAR);
	
	// Create a window called "Remap - reflection in the x direction" and show the image after applying remapping - reflection in the x direction
	imshow("Remap - reflection in the x direction", image_flippedleft);

	
	// Apply Median Filter to the Image for which histogram equalization is applied 
	medianBlur(dst, dst2, MAX_KERNEL_LENGTH);


	// Create a window called "Median Filtered Image" and show the image after applying median filtering
	imshow(window_name4, dst2);
	
    // Remove noise from the image for which histogram equalization is applied by blurring with a Gaussian filter
	GaussianBlur(dst, dst3, Size(3, 3), 0);

	// Create a window called "Gaussian Filtered Image" and show the image after applying Gaussian filtering
	imshow(window_name5, dst3);

	/// Apply Laplace function to compute the edge image using the Laplace Operator
	Laplacian(dst3, dst4, ddepth, kernel_size, scale, delta);
	convertScaleAbs(dst4, abs_dst4);

    /// Create window called "Laplace Demo" and show the edge image after applying Laplace Operator
	imshow(window_name8, abs_dst4);


	// Apply Sobel Edge Detection
	/// Appropriate variables grad, grad_x and grad_y, abs_grad_x and abs_grad_y are generated
	Mat grad;
	Mat grad_x, grad_y;
	Mat abs_grad_x, abs_grad_y;

	/// Compute Gradient X
	Sobel(dst3, grad_x, ddepth, 1, 0, 3, scale, delta, BORDER_DEFAULT);
	convertScaleAbs(grad_x, abs_grad_x);

	/// Compute Gradient Y
	Sobel(dst3, grad_y, ddepth, 0, 1, 3, scale, delta, BORDER_DEFAULT);
	convertScaleAbs(grad_y, abs_grad_y);

	/// Compute Total Gradient (approximate)
	addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad);
	
	/// Create window called "Sobel Demo - Simple Edge Detector" and show Sobel edge detected image
	imshow(window_name9, grad);

  	/// Wait until user exit program by pressing a key
	waitKey(0);

	return 0;
}