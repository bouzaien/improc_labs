#!/usr/bin/env python3

__author__ = "Mokhles Bouzaien"
__email__ = "bouzaien@eurecom.fr"

import cv2
import numpy as np

window_name0 = "Original Image"
window_name1 = "Grayscale Image"
window_name2 = "Image After Histogram Equalization"
remap_window1 = "Remap - upside down"
remap_window2 = "Remap - reflection in the x direction"
window_name4 = "Median Filtered Image"
window_name5 = "Gaussian Filtered Image"
window_name6 = "Laplace Demo"
window_name7 = "Sobel Demo - Simple Edge Detector"
window_name8 = "Laplace Demo"
window_name9 = "Sobel Demo - Simple Edge Detector"

ddepth = cv2.CV_16S
scale = 1
delta = 0
kernel_size = 3

MAX_KERNEL_LENGTH = 5

src = cv2.imread( "imgs/Baboon.jpg" )
if src is None:
    print('Could not open or find the image')
    exit(0)

# Create a window called "Original Image" and show original image
cv2.imshow(window_name0, src)

	
# Call the appropriate function in OPENCV to convert the image to grayscale
src_gray = cv2.cvtColor(src, 7) # 7 for RGB_TO_GRAY
cv2.imshow(window_name1, src_gray)
cv2.imwrite(window_name1+".jpg", src_gray) 

# Apply histogram equalization to the grayscale image
dst = cv2.equalizeHist(src_gray)
cv2.imshow(window_name2, dst)
cv2.imwrite(window_name2+".jpg", dst) 

image_upsidedown = np.array(dst.shape)
image_flippedleft = np.array(dst.shape)

rows = dst.shape[0]
cols = dst.shape[1]

map_x = np.zeros((rows, cols), dtype=np.float32)
map_y = np.zeros((rows, cols), dtype=np.float32)

for i in range(rows):
    map_x[i,:] = [x for x in range(cols)]
for j in range(map_y.shape[1]):
    map_y[:,j] = [rows-y for y in range(rows)]

image_upsidedown = cv2.remap(dst, map_x, map_y, cv2.INTER_LINEAR)
cv2.imshow(remap_window1, image_upsidedown)
cv2.imwrite(remap_window1+".jpg", image_upsidedown) 

for i in range(rows):
    map_x[i,:] = [cols-x for x in range(cols)]
for j in range(cols):
    map_y[:,j] = [y for y in range(rows)]

image_flippedleft = cv2.remap(dst, map_x, map_y, cv2.INTER_LINEAR)
cv2.imshow(remap_window2, image_flippedleft)
cv2.imwrite(remap_window2+".jpg", image_flippedleft) 

# Apply Median Filter to the Image for which histogram equalization is applied 
dst2 = cv2.medianBlur(dst, MAX_KERNEL_LENGTH)
cv2.imshow(window_name4, dst2)
cv2.imwrite(window_name4+".jpg", dst2) 

# Remove noise from the image for which histogram equalization is applied by blurring with a Gaussian filter
dst3 = cv2.GaussianBlur(dst, (3, 3), 0)
cv2.imshow(window_name5, dst3)
cv2.imwrite(window_name5+".jpg", dst3) 

#/ Apply Laplace function to compute the edge image using the Laplace Operator
dst4 = cv2.Laplacian(dst3, ddepth, ksize=kernel_size)
abs_dst4 = cv2.convertScaleAbs(dst4)
cv2.imshow(window_name8, abs_dst4)
cv2.imwrite(window_name8+".jpg", abs_dst4) 


# Apply Sobel Edge Detection

# Compute Gradient X
grad_x = cv2.Sobel(dst3, ddepth, 1, 0, ksize=3, scale=scale, delta=delta, borderType=cv2.BORDER_DEFAULT)

# Compute Gradient Y
grad_y = cv2.Sobel(dst3, ddepth, 0, 1, ksize=3, scale=scale, delta=delta, borderType=cv2.BORDER_DEFAULT)

abs_grad_x = cv2.convertScaleAbs(grad_x)
abs_grad_y = cv2.convertScaleAbs(grad_y)

# Compute Total Gradient (approximate)
grad = cv2.addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0)
cv2.imshow(window_name9, grad)
cv2.imwrite(window_name9+".jpg", grad) 


cv2.waitKey()