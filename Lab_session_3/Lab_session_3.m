% II. Pre-processing: de-noising

% A. Step 1: load the image and artificially add some noise

img = imread('ic2.tif');
noisy_image = imnoise(img,'gaussian',0.1);

figure;
subplot(1,2,1);
imshow(img);
title('Original Image');
subplot(1,2,2);
imshow(noisy_image,[]);
title('Noisy Image');

% B. Step 2: de-noise image

% Different sizes average filtering
h_1 = fspecial('average',3);
h_2 = fspecial('average',5);
h_3 = fspecial('average',7);
h_4 = fspecial('average',9);
h_5 = fspecial('average',15);

avg_filtered_image_1 = filter2(h_1,noisy_image);
avg_filtered_image_2 = filter2(h_2,noisy_image);
avg_filtered_image_3 = filter2(h_3,noisy_image);
avg_filtered_image_4 = filter2(h_4,noisy_image);
avg_filtered_image_5 = filter2(h_5,noisy_image);

figure
subplot(1,5,1);
imshow(avg_filtered_image_1,[]);
title('3x3 Average Filter');
subplot(1,5,2);
imshow(avg_filtered_image_2,[]);
title('5x5 Average Filter');
subplot(1,5,3);
imshow(avg_filtered_image_3,[]);
title('7x7 Average Filter');
subplot(1,5,4);
imshow(avg_filtered_image_4,[]);
title('9x9 Average Filter');
subplot(1,5,5);
imshow(avg_filtered_image_5,[]);
title('15x15 Average Filter');

% Different sizes median filtering
med_filtered_image_1 = medfilt2(noisy_image,[3 3]);
med_filtered_image_2 = medfilt2(noisy_image,[5 5]);
med_filtered_image_3 = medfilt2(noisy_image,[7 7]);
med_filtered_image_4 = medfilt2(noisy_image,[9 9]);
med_filtered_image_5 = medfilt2(noisy_image,[15 15]);

figure
subplot(1,5,1);
imshow(med_filtered_image_1,[]);
title('3x3 Median Filter');
subplot(1,5,2);
imshow(med_filtered_image_2,[]);
title('5x5 Median Filter');
subplot(1,5,3);
imshow(med_filtered_image_3,[]);
title('7x7 Median Filter');
subplot(1,5,4);
imshow(med_filtered_image_4,[]);
title('9x9 Median Filter');
subplot(1,5,5);
imshow(med_filtered_image_5,[]);
title('15x15 Median Filter');

% Different sizes wiener filtering
wnr_filtered_image_1 = wiener2(noisy_image,[3 3]);
wnr_filtered_image_2 = wiener2(noisy_image,[5 5]);
wnr_filtered_image_3 = wiener2(noisy_image,[7 7]);
wnr_filtered_image_4 = wiener2(noisy_image,[9 9]);
wnr_filtered_image_5 = wiener2(noisy_image,[15 15]);

figure
subplot(1,5,1);
imshow(wnr_filtered_image_1,[]);
title('3x3 Wiener Filter');
subplot(1,5,2);
imshow(wnr_filtered_image_2,[]);
title('5x5 Wiener Filter');
subplot(1,5,3);
imshow(wnr_filtered_image_3,[]);
title('7x7 Wiener Filter');
subplot(1,5,4);
imshow(wnr_filtered_image_4,[]);
title('9x9 Wiener Filter');
subplot(1,5,5);
imshow(wnr_filtered_image_5,[]);
title('15x15 Wiener Filter');

figure;
subplot(1,3,1);
imshow(avg_filtered_image_3,[]);
title('7x7 Average Filter');
subplot(1,3,2);
imshow(med_filtered_image_3,[]);
title('7x7 Median Filter');
subplot(1,3,3);
imshow(wnr_filtered_image_3,[]);
title('7x7 Wiener Filter');

% I have chosen to test filtering with 5 different sizes (3x3, 5x5, 7x7,
% 9x9 and 15x15) for each filter (average, meduan and wiener). As we can
% see in the previous results, for extreme size values, we don't get
% satisfied results : for low sizes, the noise is still present in the
% picture and for high sizez, we have totally lost some important data such
% as the edges. So the optimum filter size to choose is the 7x7. When
% comparing he different filters, we can see that Wiener filtering gave the
% best performance : delete noise and keep edges.

denoised_image = wnr_filtered_image_3;
figure;
imshow(denoised_image,[]);
title('De-noised Image');


% III. Processing: low level feature detection

% A. Step3: highlight edges

% To detect and highlight edges, we are going to use three different
% methods, compare them and choose the best one: gradient, zero crossing
% and Canny edge detector.

% 1. Gradient

% Vertical and horizontal gradients
h_filt =  [-1 0 1];
v_filt = [-1 0 1]';

h_grad = filter2(h_filt,denoised_image);
v_grad = filter2(v_filt,denoised_image);
grad = sqrt(h_grad.^2+v_grad.^2);
level = graythresh(grad);

% Here, we have applied horizontal and vertical gradients to detect
% vertical and horizontal edges then calculated the gradient magnitude.

figure
subplot(1,3,1);
imshow(h_grad,[]);
title('Horizontal gradient');
subplot(1,3,2);
imshow(v_grad,[]);
title('Vertical gradient');
subplot(1,3,3);
imshow(grad,[]);
title('Gradient magnitude');

BW = imbinarize(grad,level*255);
BW_morph = bwmorph(BW,'thin');

% Binirizing and applying morphological operation to make edges more
% visible: the edges are becoming more thin.

figure
subplot(1,2,1);
imshow(BW);
subplot(1,2,2);
imshow(BW_morph);

% 2. Zero crossing

lpn = [0 1 0;1 4 1;0 1 0];
lpn_filtered = edge(denoised_image,'zerocross',0.5,lpn);
figure; imshow(lpn_filtered,[]);

% 3. Canny edge detector

canny_filtered = edge(denoised_image,'canny');
figure; imshow(canny_filtered,[]);

figure;
subplot(1,3,1);
imshow(BW_morph,[]);
title('Gradient');
subplot(1,3,2);
imshow(lpn_filtered,[]);
title('Zero Crossing');
subplot(1,3,3);
imshow(canny_filtered,[]);
title('Canny');

% The gradient and zero crossing methods are not strong enough to detect
% edges and sometimes fooled  by noise. Canny edge detector seems to be
% more efficient.


% B. Compute the Radon transform

R = radon(canny_filtered);
figure;
subplot(1,2,1);
imshow(canny_filtered,[]);
subplot(1,2,2);
imshow(R,[]);
xlabel('\theta');
colormap(gca,hot);

% As we know, a line in the image is represented by a point (R,theta) in
% the hough transform domain, this point is the intersection of many curves
% (each curve represents a point of the image). We can clearly see several
% intersects of the curves for theta=90 which represent horizontal edges
% and for theta around 0 and 180 which represent vertical edges of the
% image.


% IV. Post-processing: high level detection & interpretation

% A. step 5: choose points in Radon transform and observe associated lines

% used interactiveLine(canny_filtered,R,3) to select horizontal and
% vertical lines.

% B. Step 6: find the image orientation and rotate it

V = max(R);
[M, i] = max(V);

VV = V(1:90) + V(91:180);
[MM, ii] = max(VV);

figure;
subplot(1,2,1);
plot(V);
subplot(1,2,2);
plot(VV);

% We are calculating VV:=V(1:90)+V(91:180) to see which two orthogonal
% directions have more edges, that means eadges in direction theta and
% edges in direction theta+90 are calculated together (for theta between 0
% and 90).
% Let us start with the V plot: as we can see there are 2 considerable
% peaks for i=88 and for i=178 that means that most edges are in those two
% directions.
% For the VV plot: we can see a single peak at i=88, that means the two
% orthogonal directions with most edges are 88 and 88+90.
% So in order to get those edges appear horizontal and vertical, we rotate
% the picture with an angle theta=-88.

rotated_image = imrotate(denoised_image,-ii);
figure;
subplot(1,2,1);
imshow(noisy_image,[]);
title('Original Image');
subplot(1,2,2);
imshow(rotated_image,[]);
title('Registered Image');
