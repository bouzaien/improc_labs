lena = imread('\\datas\teaching\courses\image\TpGettingStarted\public\Images\lena.tif');


% II.A. Squares
resized_lena = imresize(lena, 0.25);
size(resized_lena)

squares = uint8(zeros(1024,1024));
squares(1:1024,1:1024) = 32;
squares(257:257+511,257:257+511) = 64;
squares(385:385+255,385:385+255) = 128;
squares(449:449+127,449:449+127) = rgb2gray(resized_lena);

figure
imshow(squares);


% III. Mosaic
vide = uint8(zeros(size(lena,1),size(lena,2)));

G = cat(3,vide,lena(:,:,2),vide);
R = cat(3,lena(:,:,1),vide,vide);
B = cat(3,vide,vide,lena(:,:,3));

mosaic = uint8(zeros(size(lena,1)*2,size(lena,2)*2,3));
mosaic(1:512,1:512,:) = lena;

mosaic(1:512,513:1024,:) = flipdim(B ,1);
mosaic(513:1024,1:512,:)= flipdim(R ,2);
mosaic(513:1024,513:1024,:)= flipdim(flipdim(G ,2),1);
figure
imshow(mosaic);


% III.A. 3D image
halfresized_lena = imresize(lena, 0.5);
grayscaled = rgb2gray(halfresized_lena);
figure
set(surf(grayscaled),'Linestyle','none');
colormap(gray(256))


% III.B. Bitplane slicing
bitplane = uint8(zeros(size(halfresized_lena,1)*2,size(halfresized_lena,2)*4));
figure
imshow(bitplane)
% jameledd@eurecom.fr
bitplane