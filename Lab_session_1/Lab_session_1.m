% Name & Surname : Mokhles BOUZAIEN
% E-mail : mokhles.bouzaien@eurecom.fr

% Load image
lena = imread('\\datas\teaching\courses\image\TpGettingStarted\public\Images\lena.tif');
lena = imread('lena.tif');

% II.A. Squares
% Resize lena.tif with a 1/4 factor and check new size
resized_lena = imresize(lena, 0.25);
size(resized_lena)
% Create new empty image (matrix)
squares = uint8(zeros(1024,1024));
squares(1:1024,1:1024) = 32;
squares(257:257+511,257:257+511) = 64;
squares(385:385+255,385:385+255) = 128;
squares(449:449+127,449:449+127) = rgb2gray(resized_lena);

figure
imshow(squares);


% III. Mosaic
% Create new empty image
vide = uint8(zeros(size(lena,1),size(lena,2)));
% Extract each color channel
G = cat(3,vide,lena(:,:,2),vide);
R = cat(3,lena(:,:,1),vide,vide);
B = cat(3,vide,vide,lena(:,:,3));

mosaic = uint8(zeros(size(lena,1)*2,size(lena,2)*2,3));
% Concatinate the 4 images after flip
mosaic(1:512,1:512,:) = lena;
mosaic(1:512,513:1024,:) = flipdim(B ,1);
mosaic(513:1024,1:512,:)= flipdim(R ,2);
mosaic(513:1024,513:1024,:)= flipdim(flipdim(G ,2),1);

figure
imshow(mosaic);


% III.A. 3D image
% Resize lena.tif with a 1/2 factor
halfresized_lena = imresize(lena, 0.5);
grayscaled = rgb2gray(halfresized_lena);
figure
set(surf(grayscaled),'Linestyle','none');
colormap(gray(256))


% III.B. Bitplane slicing
bitplane = uint8(zeros(size(halfresized_lena,1)*2,size(halfresized_lena,2)*4));
gray_lena = rgb2gray(halfresized_lena);
% Convert decimal values to binary
b = de2bi(gray_lena);
size(b)
% Resize each 65535x1 column to 256x256 matrix and concatinate all images
bitplane(1:256,1:256) = reshape(b(:,1),256,256);
bitplane(1:256,257:512) = reshape(b(:,2),256,256);
bitplane(1:256,513:768) = reshape(b(:,3),256,256);
bitplane(1:256,769:1024) = reshape(b(:,4),256,256);
bitplane(257:512,1:256) = reshape(b(:,5),256,256);
bitplane(257:512,257:512) = reshape(b(:,6),256,256);
bitplane(257:512,513:768) = reshape(b(:,7),256,256);
bitplane(257:512,769:1024) = reshape(b(:,8),256,256);
% 0 <- 0 and 1 <- 256
bitplane = bitplane .* 256;

figure
imshow(bitplane);